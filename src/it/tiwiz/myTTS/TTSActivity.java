package it.tiwiz.myTTS;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.util.HashMap;
import java.util.Locale;

public class TTSActivity extends Activity implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener,
        TextToSpeech.OnInitListener,
        AdapterView.OnItemSelectedListener {

    SeekBar speedBar;
    TextView speedValue;
    EditText speechText;
    Button btnSpeak;
    Spinner spinner;

    TextToSpeech tts;

    static double speechSpeed = 1.0;

    static final String[] MODES = {String.valueOf(AudioManager.USE_DEFAULT_STREAM_TYPE),
            String.valueOf(AudioManager.STREAM_ALARM),
            String.valueOf(AudioManager.STREAM_DTMF),
            String.valueOf(AudioManager.STREAM_MUSIC),
            String.valueOf(AudioManager.STREAM_NOTIFICATION),
            String.valueOf(AudioManager.STREAM_RING),
            String.valueOf(AudioManager.STREAM_SYSTEM),
            String.valueOf(AudioManager.STREAM_VOICE_CALL)};

    static HashMap<String, String> myModes;

    static final String KEY = TextToSpeech.Engine.KEY_PARAM_STREAM;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        speedBar = (SeekBar) findViewById(R.id.barSpeed);
        speedBar.setOnSeekBarChangeListener(this);
        speedValue = (TextView) findViewById(R.id.lblSpeedValue);
        speechText = (EditText) findViewById(R.id.txtStringToSay);
        btnSpeak = (Button) findViewById(R.id.btnSpeak);
        btnSpeak.setOnClickListener(this);
        spinner = (Spinner) findViewById(R.id.spinnerChooseMode);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.mode_array,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        int intCurrentSpeed = speedBar.getProgress();
        String currentSpeed = getCurrentSpeedAsString(intCurrentSpeed);
        speedValue.setText(currentSpeed);

        //Passiamo in ingresso il Listener per l'Init, che è la nostra Activity
        //e l'Engine per la sintesi
        tts = new TextToSpeech(this,this);

        myModes = new HashMap<String, String>();
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btnSpeak:
                String speechString = speechText.getText().toString();
                if(speechString.length() == 0){
                    speechText.setError("Devi inserire una stringa da ripetere");
                }else{
                    speechText.setError(null);
                    speak(speechString,speechSpeed);
                }
                break;
            default: break;
        }
    }

    private double getCurrentSpeed(int seekBarValue){

        double dSeekBarValue = seekBarValue;
        speechSpeed = dSeekBarValue/10;

        return speechSpeed;
    }

    private String getCurrentSpeedAsString(int seekBarValue){

        StringBuilder sb = new StringBuilder();
        sb.append(getCurrentSpeed(seekBarValue));

        return sb.toString();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        int intCurrentSpeed = speedBar.getProgress();
        String currentSpeed = getCurrentSpeedAsString(intCurrentSpeed);
        speedValue.setText(currentSpeed);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //do nothing
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //do nothing
    }

    public void speak(String text, double speechSpeed){

        tts.setSpeechRate((float)speechSpeed);
        if(myModes.containsKey(KEY))
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myModes);
        else
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int status) {

        if(status == TextToSpeech.SUCCESS){
            int result = tts.setLanguage(Locale.ITALIAN); //impostiamo l'italiano

            if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                //errore ed uscita
                btnSpeak.setEnabled(false);
                notifica("Mancano i dati vocali: installali per continuare.");
            }else{
                btnSpeak.setEnabled(true);
            }
        }else{
            btnSpeak.setEnabled(false);
            notifica("Il Text To Speech sembra non essere supportato.");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

        if(myModes.containsKey(KEY))
            myModes.remove(KEY);

        Log.d("DEBUG", adapterView.getItemAtPosition(pos).toString());

        myModes.put(KEY,MODES[pos]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //do nothing
    }

    public void notifica(String messaggio){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Errore")
                .setCancelable(false)
                .setMessage(messaggio)
                .setPositiveButton("Esci dall'app", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TTSActivity.this.finish();
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.show();
    }
}
